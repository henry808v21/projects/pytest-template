import pytest
import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'code')))

from tagmanager import TagManager

class TestTagmanager:
    @pytest.fixture()
    def setup(self):
        """Test setup two tag managers and add 2 tags to the se"""
        self.tags1 = TagManager()
        self.tags2 = TagManager()
        self.tags2.add_tag("2 tag")
        self.tags2.add_tag("1 tag")

    def test_in_tags(self, setup):
        """Test in_tags"""
        assert not self.tags1.in_tags("1 tag")
        assert not self.tags1.in_tags("2 tag")
        assert self.tags2.in_tags("1 tag")
        assert self.tags2.in_tags("2 tag")

    def test_add_tag(self, setup):
        """Test add_tag"""
        self.tags1.add_tag("tag1")
        assert self.tags1.in_tags("tag1")
        assert not self.tags1.in_tags("tag2")
        self.tags1.add_tag("tag2")
        assert self.tags1.in_tags("tag2")

    def test_remove_tag(self, setup):
        """Test remove_tag"""
        assert self.tags2.in_tags("2 tag")
        self.tags2.remove_tag("2 tag")
        assert not self.tags2.in_tags("2 tag")

    def test_remove_tag(self, setup):
        """Test remove_tag"""
        assert self.tags2.in_tags("2 tag")
        self.tags2.remove_tag("2 tag")
        assert not self.tags2.in_tags("2 tag")

    def test_search_tags(self, setup):
        """Test search_tags"""
        tag1 = "tag"
        tags = self.tags2.search_tags(tag1)
        tags.sort()
        assert tags == ["1 tag", "2 tag"]

    def test_list_all_tags(self, setup):
        """Test list_all_tags"""
        self.tags2.add_tag("3 tag")
        tag_list = self.tags2.list_all_tags()
        assert tag_list == ["1 tag", "2 tag", "3 tag"]
