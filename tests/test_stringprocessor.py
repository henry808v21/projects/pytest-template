# Test file for stringprocessor
import pytest
import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'code')))

from stringprocessor import StringProcessor

class TestStringProcessor:
    @pytest.fixture()
    def setup(self):
        self.str_list = ["pear", "orange", 
                         "orange", "apple"]
        self.sorted = ["apple", "orange", 
                       "orange", "pear"]
        self.sp = StringProcessor
    
    def test_sort(self, setup):
        self.new_list = self.sp.sort_strings(self.str_list)
        assert self.new_list == self.sorted 

    def test_remove_duplicates(self, setup):
        assert sorted(self.sp.remove_duplicates(self.str_list)) == sorted(["pear", "orange", "apple"])

    def test_filter_by_substring(self, setup):
        self.str_list.append("pineapple")
        assert self.sp.filter_by_substring(self.str_list, "app") == ["apple", "pineapple"]
        assert not self.sp.filter_by_substring(self.str_list, "app") == self.sorted

    def test_convert_to_uppercase(self, setup):
        assert self.sp.convert_to_uppercase(self.str_list) == \
                ["PEAR", "ORANGE", "ORANGE", "APPLE"]
