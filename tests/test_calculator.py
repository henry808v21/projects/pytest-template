# tests for calculator.py

import pytest
import os
import sys


sys.path.append(os.path.normpath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', 'code')))
from calculator import Calculator



class TestCalculator:
    @pytest.fixture(autouse=True)
    def setup(self):
        self.calc = Calculator

    def test_add(self):
        """Test add"""
        assert self.calc.add(4, 5) == 9

    def test_subtract(self):
        """Test subtract"""
        assert self.calc.subtract(4, 5) == -1

    def test_multiply(self):
        """Test multiply"""
        assert self.calc.multiply(4, 5) == 20

    def test_divide(self):
        """Test divide including the divide by zero"""
        assert self.calc.divide(22, 4) == 5.5
        with pytest.raises(ValueError):
            self.calc.divide(22, 0)

    def test_power(self):
        """Test power"""
        assert self.calc.power(2, 4) == 16
        assert self.calc.power(2, -2) == 0.25
