#!/usr/bin/env python

# tag manager class

class TagManager:
    """Manages a collection of tags for categorizing content."""

    def __init__(self):
        """Initializes the TagManager with an empty list of tags."""
        self.tags = []

    def in_tags(self, tag):
        """Removes a tag from the collection."""
        return tag in self.tags

    def add_tag(self, tag):
        """Adds a new tag to the collection if it is not already present."""
        if not self.in_tags(tag):
            self.tags.append(tag)
            self.tags.sort()

    def remove_tag(self, tag):
        """Removes a tag from the collection."""
        if tag in self.tags:
            self.tags.remove(tag)

    def search_tags(self, substring):
        """Returns tags that contain the specified substring."""
        return [tag for tag in self.tags if substring in tag]

    def list_all_tags(self):
        """Returns all tags sorted alphabetically."""
        return sorted(self.tags)
