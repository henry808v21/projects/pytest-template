#!/usr/bin/env python

# stringprocessor class

class StringProcessor:
    """A class for processing lists of strings."""

    @staticmethod
    def sort_strings(strings):
        """Sorts a list of strings alphabetically."""
        return sorted(strings)

    @staticmethod
    def remove_duplicates(strings):
        """Removes duplicate strings from the list."""
        return list(set(strings))

    @staticmethod
    def filter_by_substring(strings, substring):
        """Returns strings containing a specific substring."""
        return [s for s in strings if substring in s]

    @staticmethod
    def convert_to_uppercase(strings):
        """Converts all strings in the list to uppercase."""
        return [s.upper() for s in strings]