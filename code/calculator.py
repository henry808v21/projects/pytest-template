#!/usr/bin/env python

# Calculator class

class Calculator:
    """Simple calculator class capable of performing basic arithmetic operations."""

    @staticmethod
    def add(x, y):
        """Return the sum of x and y."""
        return x + y

    @staticmethod
    def subtract(x, y):
        """Return the difference of x and y."""
        return x - y

    @staticmethod
    def multiply(x, y):
        """Return the product of x and y."""
        return x * y

    @staticmethod
    def divide(x, y):
        """Return the quotient of x and y. Raises ValueError on division by zero."""
        if y == 0:
            raise ValueError("Cannot divide by zero.")
        return x / y

    @staticmethod
    def power(x, y):
        """Return x raised to the power of y."""
        return x ** y

if __name__ == "__main__":
    # Create a calculator instance
    calc = Calculator()

    # Demonstrating some uses of the calculator
    print("Addition of 5 and 3:", calc.add(5, 3))
    print("Subtraction of 5 from 10:", calc.subtract(10, 5))
    print("Multiplication of 4 and 5:", calc.multiply(4, 5))
    print("Division of 20 by 5:", calc.divide(20, 5))
    print("Power of 2 raised to 3:", calc.power(2, 3))
