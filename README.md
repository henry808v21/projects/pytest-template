# pytest-template

## Dependencies
pytest
```bash
conda install pytest

```

## git
```bash
cd existing_repo
git remote add origin https://gitlab.com/henry808v21/projects/pytest-template.git
git branch -M main
git push -uf origin main
```

## conda
```bash
conda create -n pytest-template python=3.11

# To activate this environment, use
conda activate pytest-template

# To deactivate an active environment, use
conda deactivate
```

## Name
pytest template

## Description
Standard templates for pytest set up in a working tree with the tests in a  sibling directory.

### test_calculator
Test for a class with static only methods

### test_tagmanager.py
Test for a class with objects



## Badges



## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Henry Grantham

## License
For open source projects, say how it is licensed.

## Project status
In progress
